def vowel_swapper(string):
    # ==============
    # Your code here
    vowel_replacer = {'a': '4',
                      'e': '3',
                      'i': '!',
                      'o': 'ooo',
                      'u': '|_|'}

    for key, value in vowel_replacer.items():
        string = string.lower().replace(key, value)

    return string
    # ==============

print(vowel_swapper("aA eE iI oO uU")) # Should print "44 33 !! ooo000 |_||_|" to the console
print(vowel_swapper("Hello World")) # Should print "H3llooo Wooorld" to the console 
print(vowel_swapper("Everything's Available")) # Should print "3v3ryth!ng's 4v4!l4bl3" to the console